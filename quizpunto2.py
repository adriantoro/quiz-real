#Crear un script de python que, dados dos diccionarios, genere un tercero con la suma de los items que tengan la misma llave.

def sum_dicts(diccio1, diccio2):
  #Crear un script de python que, dados dos diccionarios, genere un tercero con la suma de los items que tengan la misma llave.  
    
    final_diccio ={}
    
    for key1, valor1 in diccio1.items():
        for key2, valor2 in diccio2.items():
            if key1 ==key2 :
                final_diccio.setdefault (key1,valor1+valor2)
                break
    return final_diccio

number_diccio1 ={
    "a":2,
    "b":1,
    "c":3,
}
number_diccio2 = {
    "d": 4,
    "e": 5,
    "f" : 6
}

print (sum_dicts(number_diccio1,number_diccio2))
            


